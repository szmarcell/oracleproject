BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   job_name             => 'monthly_sales',
   job_type             => 'STORED_PROCEDURE',
   job_action           => 'SALES_PKG.UPDATE_SALES_SUMMARY',
   start_date           =>  SYSDATE + 1,
   repeat_interval      => 'FREQ=WEEKLY', 
   end_date             =>  SYSDATE + 30,
   enabled              =>  TRUE,
   comments             => 'Some specific products''s price will decrease over the next month');
END;
/
