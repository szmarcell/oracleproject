PROMPT Creating tables...

ALTER SESSION SET CURRENT_SCHEMA=KAMELEON_OWNER;

PROMPT Creating departments table...

CREATE TABLE departments(
  dept_id        NUMBER          NOT NULL
  ,dept_name     VARCHAR2(100)   NOT NULL
  ,tax_number    VARCHAR2(100)   NOT NULL
  ,loc_address   VARCHAR2(100)   NOT NULL
  ,loc_city      VARCHAR2(100)        
  ,loc_region    VARCHAR2(100)
  ,postal_code   NUMBER
  ,country       VARCHAR2(50)
  ,tel           VARCHAR2(20)
  ,email         VARCHAR2(50)
)
TABLESPACE kameleon_tablespace;

ALTER TABLE departments
  ADD CONSTRAINT dept_pk PRIMARY KEY (dept_id);
  
CREATE INDEX department_ix_01 ON departments(loc_address);
  
COMMENT ON TABLE departments IS 'The company''s premises';
COMMENT ON COLUMN departments.dept_id IS 'The ID of the departments';
COMMENT ON COLUMN departments.tax_number IS 'The tax number of the department';
COMMENT ON COLUMN departments.loc_address IS 'The address location of the partner';
COMMENT ON COLUMN departments.loc_city IS 'The city of the partner''s location';
COMMENT ON COLUMN departments.loc_region IS 'The region of the partner';
COMMENT ON COLUMN departments.postal_code IS 'The postal code of the partners location';
COMMENT ON COLUMN departments.country IS 'The country of the partner''s location';
COMMENT ON COLUMN departments.tel IS 'The telephone number of the partner';
COMMENT ON COLUMN departments.email IS 'The email address of the partner';

CREATE SYNONYM premises FOR departments;

PROMPT Table departments created.

PROMPT Creating employees table...

CREATE TABLE employees(
  employee_id     NUMBER          NOT NULL
  ,dept_id        NUMBER          NOT NULL
  ,last_name      VARCHAR2(40)    NOT NULL
  ,first_name     VARCHAR2(40)    NOT NULL
  ,title          VARCHAR2(40)    NOT NULL
  ,birth_date     DATE
  ,hire_date      DATE
  ,loc_address    VARCHAR2(100)
  ,loc_city       VARCHAR2(100)   NOT NULL
  ,postal_code    NUMBER
)
TABLESPACE kameleon_tablespace;

ALTER TABLE employees
  ADD CONSTRAINT employee_pk PRIMARY KEY (employee_id);
  
ALTER TABLE employees
  ADD CONSTRAINT employee_dept_fk FOREIGN KEY (dept_id) REFERENCES departments(dept_id);
  
CREATE INDEX employee_ix_01 ON employees(last_name, first_name, birth_date);
CREATE INDEX employee_ix_02 ON employees(loc_address, loc_city);
  
COMMENT ON TABLE employees IS 'The workers of the company';
COMMENT ON COLUMN employees.employee_id IS 'The ID of the employee';
COMMENT ON COLUMN employees.dept_id IS 'The foreign ID of the department';
COMMENT ON COLUMN employees.last_name IS 'Last name of the employee';
COMMENT ON COLUMN employees.first_name IS 'First name of the employee';
COMMENT ON COLUMN employees.title IS 'The title of the employee';
COMMENT ON COLUMN employees.birth_date IS 'Birthdate of the employee';
COMMENT ON COLUMN employees.hire_date IS 'Hiredate of the employee';
COMMENT ON COLUMN employees.loc_address IS 'The address of the employee''s location';
COMMENT ON COLUMN employees.loc_city IS 'The city of the employee''s location';
COMMENT ON COLUMN employees.postal_code IS 'The postal code of the employee''s location';

CREATE SYNONYM workers FOR employees;

PROMPT Table departments created.

PROMPT Creating partners table...

CREATE TABLE partners(
  partner_id     NUMBER           NOT NULL
  ,partner_name  VARCHAR2(100)    NOT NULL
  ,tax_number    VARCHAR2(100)    NOT NULL
  ,loc_address   VARCHAR2(100)    
  ,loc_city      VARCHAR2(100)        
  ,loc_region    VARCHAR2(100)
  ,postal_code   NUMBER
  ,country       VARCHAR2(50)
  ,tel           VARCHAR2(20)
  ,email         VARCHAR2(50)
)
TABLESPACE kameleon_tablespace;

ALTER TABLE partners
  ADD CONSTRAINT partner_pk PRIMARY KEY (partner_id);
  
COMMENT ON TABLE partners IS 'The company''s partners / regular customers';
COMMENT ON COLUMN partners.partner_id IS 'The ID of the partner';
COMMENT ON COLUMN partners.tax_number IS 'The tax number of the partner/customer';
COMMENT ON COLUMN partners.partner_name IS 'The name of the partner';
COMMENT ON COLUMN partners.loc_address IS 'The address location of the partner';
COMMENT ON COLUMN partners.loc_city IS 'The city of the partner''s location';
COMMENT ON COLUMN partners.loc_region IS 'The region of the partner';
COMMENT ON COLUMN partners.postal_code IS 'The postal code of the partners location';
COMMENT ON COLUMN partners.country IS 'The country of the partner''s location';
COMMENT ON COLUMN partners.tel IS 'The telephone number of the partner';
COMMENT ON COLUMN partners.email IS 'The email address of the partner';

CREATE SYNONYM customers FOR partners;

PROMPT Table partners created.

PROMPT Creating orders table...

CREATE TABLE orders(
  order_id       NUMBER           NOT NULL
  ,partner_id    NUMBER           NOT NULL
  ,employee_id   NUMBER           NOT NULL
  ,purchase_date DATE             NOT NULL
)
TABLESPACE kameleon_tablespace;

ALTER TABLE orders
  ADD CONSTRAINT order_pk PRIMARY KEY (order_id);
  
ALTER TABLE orders
  ADD CONSTRAINT order_partner_fk FOREIGN KEY (partner_id) REFERENCES partners(partner_id);

ALTER TABLE orders
  ADD CONSTRAINT order_employee_fk FOREIGN KEY (employee_id) REFERENCES employees(employee_id);
  
COMMENT ON TABLE orders IS 'The orders table';
COMMENT ON COLUMN orders.order_id IS 'The ID of the order';
COMMENT ON COLUMN orders.partner_id IS 'The ID of the partner';
COMMENT ON COLUMN orders.employee_id IS 'The ID of the employee';
COMMENT ON COLUMN orders.purchase_date IS 'The date of the purchase_date';

PROMPT Table orders created.

PROMPT Creating categories table...
  
CREATE TABLE categories(
  category_id    NUMBER            NOT NULL
  ,category_name VARCHAR2(100)     NOT NULL
  ,cat_descript  VARCHAR2(150)     
)
TABLESPACE kameleon_tablespace;

ALTER TABLE categories
  ADD CONSTRAINT category_pk PRIMARY KEY (category_id);
  
COMMENT ON TABLE categories IS 'The product categories';
COMMENT ON COLUMN categories.category_id IS 'The ID of the category';
COMMENT ON COLUMN categories.category_name IS 'The name of the category';
COMMENT ON COLUMN categories.cat_descript IS 'The description of the category';

PROMPT Table categories created.

PROMPT Creating suppliers table...

CREATE TABLE suppliers(
  supplier_id     NUMBER             NOT NULL
  ,company_name   VARCHAR2(100)      NOT NULL
  ,tax_number     VARCHAR2(30)       NOT NULL
  ,loc_address    VARCHAR2(100)    
  ,loc_city       VARCHAR2(100)        
  ,loc_region     VARCHAR2(100)
  ,postal_code    NUMBER
  ,country        VARCHAR2(50)
  ,tel            VARCHAR2(20)
  ,email          VARCHAR2(50)
)
TABLESPACE kameleon_tablespace;

ALTER TABLE suppliers
  ADD CONSTRAINT supplier_pk PRIMARY KEY (supplier_id);
  
COMMENT ON TABLE suppliers IS 'The details of the suppliers';
COMMENT ON COLUMN suppliers.supplier_id IS 'The ID of the supplier';
COMMENT ON COLUMN suppliers.company_name IS 'The name of the company';
COMMENT ON COLUMN suppliers.tax_number IS 'The tax number of the supplier';
COMMENT ON COLUMN suppliers.loc_address IS 'The address of the supplier''s location';
COMMENT ON COLUMN suppliers.loc_city IS 'The city of the supplier''s location';
COMMENT ON COLUMN suppliers.loc_region IS 'The region of the supplier''s location';
COMMENT ON COLUMN suppliers.postal_code IS 'The postal code of the supplier''s location';
COMMENT ON COLUMN suppliers.country IS 'The country of the supplier';
COMMENT ON COLUMN suppliers.tel IS 'The telephone number of the supplier';
COMMENT ON COLUMN suppliers.email IS 'The email address of the supplier';

CREATE SYNONYM provider FOR suppliers;

PROMPT Table suppliers created.
  
CREATE TABLE products(
  product_id            NUMBER            NOT NULL
  ,supplier_id          NUMBER            NOT NULL
  ,category_id          NUMBER            NOT NULL
  ,product_name         VARCHAR2(100)     NOT NULL
  ,quantity_per_unit    NUMBER(5)         NOT NULL
  ,unit_price           NUMBER(19,4)      NOT NULL
  ,units_in_stock       NUMBER(5)         
)
TABLESPACE kameleon_tablespace;

ALTER TABLE products
  ADD CONSTRAINT product_pk PRIMARY KEY (product_id);

ALTER TABLE products
  ADD CONSTRAINT product_supplier_fk FOREIGN KEY (supplier_id) REFERENCES suppliers(supplier_id);

ALTER TABLE products
  ADD CONSTRAINT product_category_fk FOREIGN KEY (category_id) REFERENCES categories(category_id);
  
COMMENT ON TABLE products IS 'The products marketed by the company';
COMMENT ON COLUMN products.product_id IS 'The ID of the product';
COMMENT ON COLUMN products.supplier_id IS 'The foreign ID of the supplier';
COMMENT ON COLUMN products.category_id IS 'The foreign ID of the category';
COMMENT ON COLUMN products.product_name IS 'The name of the product';
COMMENT ON COLUMN products.quantity_per_unit IS 'The quantity of one unit of the product';
COMMENT ON COLUMN products.unit_price IS 'The unitprice of the product';
COMMENT ON COLUMN products.units_in_stock IS 'The quantity of the product in stock';

PROMPT Table products created.

PROMPT Creating order_details table...

CREATE TABLE order_details(
  order_det_id   NUMBER            NOT NULL
  ,order_id      NUMBER            NOT NULL
  ,product_id    NUMBER            NOT NULL
  ,unit_price    NUMBER(19,4)      NOT NULL
  ,quantity      NUMBER(5)         NOT NULL
  ,discount      NUMBER(5)         DEFAULT 0 NOT NULL
)
TABLESPACE kameleon_tablespace;

ALTER TABLE order_details
  ADD CONSTRAINT order_details_order_fk FOREIGN KEY (order_id) REFERENCES orders(order_id);
  
ALTER TABLE order_details
  ADD CONSTRAINT order_details_poduct_fk FOREIGN KEY (product_id) REFERENCES products(product_id);
  
COMMENT ON TABLE order_details IS 'The details of the orders';
COMMENT ON COLUMN order_details.order_id IS 'The ID of the order';
COMMENT ON COLUMN order_details.product_id IS 'The ID of the product';
COMMENT ON COLUMN order_details.unit_price IS 'The unitprice of the product';
COMMENT ON COLUMN order_details.quantity IS 'The ordered quantity of the product';
COMMENT ON COLUMN order_details.discount IS 'The discount of the product';

PROMPT Table order_details created

PROMPT Creation of all table done.

PROMPT Creation of error_log table with its own seq...

CREATE TABLE error_log(
       err_id      NUMBER,
       err_time    TIMESTAMP DEFAULT SYSDATE,
       err_message VARCHAR2(4000),
       err_value   VARCHAR2(4000),
       api         VARCHAR2(100)
)
TABLESPACE kameleon_tablespace;
/

CREATE SEQUENCE err_seq START WITH 1;
/


