﻿PROMPT Loading tabla data...

PROMPT Loading suppliers table...

INSERT INTO suppliers (company_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) values ('Anda Kft.', '10631609-2-14', 'Verseny u. 25/1', 'Pécs', 'Baranya', 7622, 'Magyarország', '0672/213-974', 'festekaruhaz.pecs@anda.hu');
INSERT INTO suppliers (company_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) values ('Autocolor Kft.', '11224220-2-14', ' Izzó u. 5.', 'Kaposvár', 'Somogy', 7400, 'Magyarország', '0682/512-900', 'autocolor@koto.hu');
INSERT INTO suppliers (company_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) values ('Bárdi Autó Zrt.', '12229132-2-44', 'Farkas István u. 2.', 'Pécs', 'Baranya', 7626, 'Magyarország', '0672/222-523', 'vevoszolgalat@bardiauto.hu');
INSERT INTO suppliers (company_name, tax_number, loc_city, loc_region, postal_code, country) values ('Gyimesi Sándor EV.', '43420315-2-22', 'Kaposvár', 'Somogy', 7400, 'Magyarország');
INSERT INTO suppliers (company_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) values ('Basacolors Kft.', '23116560-2-43', 'Kossuth út Ipartelep HRSZ 219/7', 'Gyermely', 'Komárom-Esztergom', 2821, 'Magyarország', '0630/575-6115', 'info@basacolors.hu');
INSERT INTO suppliers (company_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) values ('CarSystem Hungária Kft.', '11136028-2-08', 'Régi Veszprémi út 14', 'Győr', 'Győr-Moson-Sopron', 9028, 'Magyarország', '0696/517-624', 'info@carsystem.hu');
INSERT INTO suppliers (company_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) values ('Car-Color Kft.', '12125021-2-43', 'Vas Gereben utca 4/d', 'Budapest', 'Pest', 1195, 'Magyarország', '061/348-0324', 'carcolor@carcolor.hu');
INSERT INTO suppliers (company_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) values ('Casati Color Kft.', '12282359-2-13','Ipartelep 51.', 'Taksony', 'Pest', 2335, 'Magyarország', '0624/536-265', 'casati@casati.hu');

PROMPT Loading suppliers table DONE.

PROMPT Loading categories table...

INSERT INTO categories (category_name, cat_descript) VALUES ('segédeszközök', 'egyéb használati tárgyak házfelújítás céljából');
INSERT INTO categories (category_name, cat_descript) VALUES ('festékek', 'különzöző felhasználási célra szánt festékek');
INSERT INTO categories (category_name, cat_descript) VALUES ('segédanyagok', 'a festékekhez szolgáló segédanyagok');
INSERT INTO categories (category_name, cat_descript) VALUES ('építkezés', 'építkezési anyagok');

PROMPT Loading categories table DONE.

PROMPT Loading products table...

INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1000, 1008, 'Sárga ecset 2,5"', 2, 200, 400);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1000, 1009, 'Beltéri falfesték 5L Fekete', 1, 4500, 30);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1000, 1011, 'Rigipsz 25Kg', 1, 3200, 35);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1001, 1008, 'Piros ecset 2,5"', 2, 220, 400);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1001, 1009, 'Beltéri falfesték 10L Hupikék', 1, 8500, 30);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1001, 1011, 'Rigipsz 20Kg', 1, 4000, 20);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1002, 1008, 'Kék ecset 3"', 2, 300, 350);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1002, 1009, 'Kültéri festék 5L Sárga', 1, 6000, 30);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1002, 1011, 'Rigipsz 30Kg', 1, 4200, 20);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1003, 1008, 'Vékony ecset 1,5"', 2, 180, 500);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1003, 1008, 'Festőhenger kék 18cm', 1, 500, 15);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1003, 1009, 'Autófesték kikevert 100ml', 1, 1500, 100);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1004, 1008, 'Mester ecset 0,5"', 4, 600, 200);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1004, 1008, 'Henger oldószerálló', 1, 800, 20); 
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1004, 1009, 'Beltéri falfesték 5L Fekete', 1, 6000, 20);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1005, 1009, 'Lazúr festék 5L dió', 1, '5000', 10);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1005, 1010, 'Nitro higító 1L', 1, 850, 30);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1005, 1010, 'Szintetikus higító 1L', 1, 1000, 20);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1006, 1010, 'Lakkbenzin higító 1L', 1, 900, 20);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1006, 1009, 'Dunaplast kerítésfesték 5L Fekete', 1, 5000, 20);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1006, 1010, 'Edző 100ml', 1, 150, 50);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1007, 1010, 'Lakk 100ml', 1, 120, 50);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1007, 1009, 'Útjelzőfesték piros 150ml', 1, 1500, 25);
INSERT INTO PRODUCTS (supplier_id, category_id, product_name, quantity_per_unit, unit_price, units_in_stock) VALUES (1007, 1010, 'Homlokzatfesték 5L fehér', 1, 4500, 20);

PROMPT Loading products table DONE.

PROMPT Loading partners table...

INSERT INTO PARTNERS (partner_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('"Bella" Méteráru Lakástextil Kft.', '12409107-2-14','Kisfaludy u. 11.', 'Kaposvár', 'Somogy', 7400, 'Magyarország', '0682/525-956', 'bella@bella.hu');
INSERT INTO PARTNERS (partner_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('ACSA 2001 Kft.', '12708619-2-14', 'Petőfi Sándor u. 163.', 'Somogyacsa', 'Somogy', 7283, 'Magyarország', '0682/585-452', 'info@acsa.hu');
INSERT INTO PARTNERS (partner_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('ÁDÁM JÓZSEF EV.', '67937073-2-27', 'Márványkert 14.', 'Iszkaszentgyörgy', 'Fejér', 8043, 'Magyarország', '0670/125-8548', 'adamjozsef@citromail.hu');
INSERT INTO PARTNERS (partner_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('AGRÁRIA ZRT.', '12588466-2-14', 'Kossuth u.1.', 'Szentgáloskér', 'Somogy', 7465, 'Magyarország', '0682/525-854', 'agraria@gmail.com');
INSERT INTO PARTNERS (partner_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('AGRICOLA 2011. KFT.', '23174861-2-14', 'Rákóczi u. 8.', 'Orci', 'Somogy', 7461, 'Magyarország', '0682/998-844', 'info@agricola.hu');
INSERT INTO PARTNERS (partner_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('ANGYAL BALÁZS EV.', '67090228-1-34', 'Hegyalja utca 80.', 'Kaposvár', 'Somogy', 7400, 'Magyarország', '0630/3258-8448', 'angyalbazsa@gmail.com');
INSERT INTO PARTNERS (partner_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('Árpádföldi Gyógyszertár Kft.', '14361346-2-13', 'Szabadka utca 16. 5.', 'Dunakeszi', 'Pest', 2120, 'Magyarország', '0630/547-9635', 'info@arpadgyogy.hu');
INSERT INTO PARTNERS (partner_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('AUTÓ - ANDOR KFT.', '13001733-2-18', 'Áfonya u. 4.', 'Szombathely', 'Vas', 9700, 'Magyarország', '0670/517-7954', 'andor@andor.hu');
INSERT INTO PARTNERS (partner_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('AUTRAS KFT.', '10622089-2-14', 'Hoszzúfalvi u. 143.', 'Lábod', 'Somogy', 7551, 'Magyarország', '0670/857-7463', 'info@autras.hu');
INSERT INTO PARTNERS (partner_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('BABU - CAR KFT.', '23383368-2-17', 'Babits M. u. 41.', 'Dombóvár', 'Somogy', 7200, 'Magyarország', '0620/698-8267', 'babu@gmail.com');

PROMPT Loading partners table DONE.

PROMPT Loading departments table...

INSERT INTO departments (dept_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('Kaméleon Paint Kft.', '14627268-2-14', 'Mező utca 4.', 'Kaposvár', 'Somogy', 7400, 'Magyarország', '0670/425-8596', 'kameleon.kft@gmail.com');
INSERT INTO departments (dept_name, tax_number, loc_address, loc_city, loc_region, postal_code, country, tel, email) VALUES ('Szöllősi Car Kft.', '26089757-2-14', 'Füredi út 67.', 'Kaposvár', 'Somogy', 7400, 'Magyarország', '0670/425-8595', 'szolloscar@gmail.com');

PROMPT Loading departments table DONE.

PROMPT Loading employees table...

INSERT INTO employees (dept_id, last_name, first_name, title, birth_date, hire_date, loc_address, loc_city, postal_code) values (1046, 'Szöllősiné Farkas', 'Katalin', 'CO-Founder', to_date('1975/11/25', 'yyyy/mm/dd'), to_date('2008/09/09', 'yyyy/mm/dd'), 'Mező utca 4.', 'Kaposvár', 7400);
INSERT INTO employees (dept_id, last_name, first_name, title, birth_date, hire_date, loc_address, loc_city, postal_code) values (1047, 'Szöllősi', 'Csaba', 'CO-Founder', to_date('1966/11/16', 'yyyy/mm/dd'), to_date('2008/09/09', 'yyyy/mm/dd'), 'Mező utca 4.', 'Kaposvár', 7400);
INSERT INTO employees (dept_id, last_name, first_name, title, birth_date, hire_date, loc_city, postal_code) values (1046, 'Németh', 'Zita', 'seller', to_date('1985/11/14', 'yyyy/mm/dd'), to_date('2012/03/24', 'yyyy/mm/dd'), 'Kaposvár', 7400);
INSERT INTO employees (dept_id, last_name, first_name, title, birth_date, hire_date, loc_city, postal_code) values (1047, 'Szabó', 'Dániel', 'seller', to_date('1989/09/19', 'yyyy/mm/dd'), to_date('2014/04/19', 'yyyy/mm/dd'), 'Böszénfa', 7475);

PROMPT Loading employees table DONE.

PROMPT Loading orders table...

INSERT INTO orders (partner_id, employee_id, purchase_date) VALUES (1036, 1048, to_date('02-09-2018 15:19:36', 'dd-mm-yyyy hh24:mi:ss'));
INSERT INTO orders (partner_id, employee_id, purchase_date) VALUES (1040, 1048, to_date('07-01-2019 09:20:59', 'dd-mm-yyyy hh24:mi:ss'));
INSERT INTO orders (partner_id, employee_id, purchase_date) VALUES (1042, 1049, to_date('02-01-2019 08:45:20', 'dd-mm-yyyy hh24:mi:ss'));
INSERT INTO orders (partner_id, employee_id, purchase_date) VALUES (1043, 1050, to_date('05-12-2018 14:12:15', 'dd-mm-yyyy hh24:mi:ss'));
INSERT INTO orders (partner_id, employee_id, purchase_date) VALUES (1045, 1051, to_date('02-11-2018 11:55:10', 'dd-mm-yyyy hh24:mi:ss'));

PROMPT Loading orders table DONE.

PROMPT Loading order_details table...

INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1052, 1012, 200, 5, 5);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1052, 1013, 4500, 6, 10);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1052, 1014, 3200, 4, 5);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1052, 1015, 220, 2, 10);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1052, 1016, 8500, 1, 0);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1053, 1017, 4000, 6, 0);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1053, 1018, 300, 7, 15);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1053, 1019, 6000, 3, 0);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1053, 1020, 4200, 4, 0);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1053, 1021, 180, 6, 10);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1054, 1022, 500, 2, 5);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1054, 1023, 1500, 1, 10);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1054, 1024, 600, 5, 0);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1054, 1025, 800, 2, 5);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1054, 1026, 6000, 1, 5);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1055, 1027, 5000, 3, 5);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1055, 1028, 850, 4, 10);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1055, 1029, 1000, 6, 5);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1055, 1030, 900, 7, 5);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1055, 1031, 5000, 5, 0);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1056, 1032, 150, 2, 0);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1056, 1033, 120, 3, 15);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1056, 1034, 1500, 4, 0);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1056, 1035, 4500, 2, 10);
INSERT INTO order_details (order_id, product_id, unit_price, quantity, discount) VALUES (1056, 1030, 900, 2, 5);

PROMPT Loading order_details table DONE.
