PROMPT Creating history tables...

ALTER SESSION SET CURRENT_SCHEMA = KAMELEON_OWNER;

------------------------------------------------------
-- Creates a history table for all table for audit  --
------------------------------------------------------
DECLARE
  CURSOR cur IS
    SELECT 'CREATE TABLE ' || t.TABLE_NAME || '_HIST AS SELECT * FROM ' || t.TABLE_NAME || ' WHERE 1 = 2' AS COMMAND
      FROM ALL_TABLES t 
      WHERE t.OWNER = 'KAMELEON_OWNER';
BEGIN
  FOR c IN cur LOOP
    EXECUTE IMMEDIATE c.COMMAND;
  END LOOP;
END;
/

PROMPT Creating history tables DONE.

PROMPT Creating audit colums for all tables...

---------------------------------------------------------------
-- Creates an audit columns for all table, comments included --
---------------------------------------------------------------
DECLARE
  CURSOR cur IS
    SELECT 'ALTER TABLE ' || t.TABLE_NAME || ' ADD (mod_user VARCHAR2(300), created_on TIMESTAMP, last_mod TIMESTAMP, DML_FLAG VARCHAR2(1), VERSION NUMBER)' AS COMMAND,
           'COMMENT ON COLUMN ' || t.TABLE_NAME || '.mod_user IS ''The user of the modification''' AS MOD_USER_COMMENT,
           'COMMENT ON COLUMN ' || t.TABLE_NAME || '.created_on IS ''The time of creation''' AS CREATED_ON_COMMENT,
           'COMMENT ON COLUMN ' || t.TABLE_NAME || '.last_mod IS ''The time of the last modification''' AS LAST_MOD_COMMENT,
           'COMMENT ON COLUMN ' || t.TABLE_NAME || '.DML_FLAG IS ''The data manipulation type''' AS DML_FLAG_COMMENT,
           'COMMENT ON COLUMN ' || t.TABLE_NAME || '.VERSION IS ''The version of the item''' AS VERSION_COMMENT
      FROM ALL_TABLES t
      WHERE t.OWNER = 'KAMELEON_OWNER';
BEGIN
  FOR c IN cur LOOP
    EXECUTE IMMEDIATE c.COMMAND;
    EXECUTE IMMEDIATE c.MOD_USER_COMMENT;
    EXECUTE IMMEDIATE c.CREATED_ON_COMMENT;
    EXECUTE IMMEDIATE c.LAST_MOD_COMMENT;
    EXECUTE IMMEDIATE c.DML_FLAG_COMMENT;
    EXECUTE IMMEDIATE c.VERSION_COMMENT;
  END LOOP;
END;
/

PROMPT Creating audit columns for all tables DONE.

PROMPT Creating sequence for database...

----------------------------------------------------------------------------------------------------------
-- Creates a sequence for the whole project, all audit will use this for the purpose of true uniqueness --
----------------------------------------------------------------------------------------------------------
CREATE SEQUENCE kameleon_seq START WITH 1000;
/

PROMPT Sequence created.

PROMPT Creating triggers for audit columns for all tables...

--------------------------------------------------------------------------------------------------
-- Creates a view table containing the insertion statements and columns the audit triggers need --
--------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW vw_history_statements AS
SELECT u.TABLE_NAME, 'INSERT INTO ' || u.TABLE_NAME || '_HIST (' || LISTAGG(u.COLUMN_NAME, ', ') WITHIN GROUP (ORDER BY u.COLUMN_ID) || ')'
                  || ' VALUES (' || REGEXP_REPLACE(LISTAGG(':old.' || u.COLUMN_NAME, ', ') WITHIN GROUP (ORDER BY u.COLUMN_ID),
                     ',([^,]+,[^,]+)$', ', ''D'', :old.VERSION') || ')' AS INSERT_DELETED,
                     'INSERT INTO ' || u.TABLE_NAME || '_HIST (' || LISTAGG(u.COLUMN_NAME, ', ') WITHIN GROUP (ORDER BY u.COLUMN_ID) || ')'
                  || ' VALUES (' || LISTAGG(':new.' || u.COLUMN_NAME, ', ') WITHIN GROUP (ORDER BY u.COLUMN_ID) || ')' AS INSERT_ELSE
FROM USER_TAB_COLS u
GROUP BY u.TABLE_NAME
HAVING NOT REGEXP_LIKE(u.TABLE_NAME, '^\w+HIST$') AND NOT REGEXP_LIKE(u.TABLE_NAME, '^\w+LOG$') AND NOT REGEXP_LIKE(u.TABLE_NAME, '^VW_') AND MIN(u.COLUMN_ID) = 1;
/

CREATE OR REPLACE VIEW vw_table_keys AS
SELECT u.TABLE_NAME, u.COLUMN_NAME
FROM USER_TAB_COLS u
GROUP BY u.TABLE_NAME, u.COLUMN_NAME
HAVING NOT REGEXP_LIKE(u.TABLE_NAME, '^\w+HIST$') AND NOT REGEXP_LIKE(u.TABLE_NAME, '^\w+LOG$') AND NOT REGEXP_LIKE(u.TABLE_NAME, '^VW_') AND MIN(u.COLUMN_ID) = 1;
/

--------------------------------------------------------
-- Automatically creates audit triggers for all table --
--------------------------------------------------------
DECLARE
  CURSOR col_cursor IS
    SELECT '
      CREATE OR REPLACE TRIGGER ' || vw.TABLE_NAME || '_trg' || '
        BEFORE INSERT OR UPDATE ON ' || vw.TABLE_NAME || '
        FOR EACH ROW
      BEGIN
        IF inserting THEN
          IF :new.' || vw.COLUMN_NAME || ' IS NULL THEN
             :new.' || vw.COLUMN_NAME || ' := kameleon_seq.nextval;
          END IF;
        
          :new.created_on := sysdate;
          :new.DML_FLAG   := ''I'';
          :new.VERSION    := 1;
        ELSIF updating THEN
          :new.DML_FLAG := ''U'';
          :new.VERSION  := :old.VERSION + 1;
        END IF;

        :new.last_mod := sysdate;
        :new.mod_user := sys_context(''USERENV'', ''OS_USER'');
      END;' AS COMMAND
    FROM vw_table_keys vw;
  CURSOR hist_cursor IS
    SELECT '
      CREATE OR REPLACE TRIGGER ' || vw.TABLE_NAME || '_htrg' || '
        AFTER DELETE OR INSERT OR UPDATE ON ' || vw.TABLE_NAME || '
        FOR EACH ROW
      BEGIN
        IF DELETING THEN ' ||
          vw.INSERT_DELETED || ';
        ELSE ' ||
          vw.INSERT_ELSE || ';
        END IF;
      END;' AS COMMAND
    FROM vw_history_statements vw;
BEGIN
  FOR c IN col_cursor LOOP
    EXECUTE IMMEDIATE c.COMMAND; -- This trigger will work on the audit columns
  END LOOP;
  
  FOR c IN hist_cursor LOOP
    EXECUTE IMMEDIATE c.COMMAND; -- This trigger will work on the audit columns
  END LOOP;
END;
/

PROMPT Creating triggers for audit columns for all tables DONE.

PROMPT Creating triggers for tables to history tables audit...










