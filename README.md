# oracleproject

Welcome to the oracleproject!

This application is a database backend of an accounting/invoice software, containing all the scripts to create a basic environment with a basic service layer.

This little project was my exam of a university course called "Oracle Databases".

I hope you like it and feel free to share your opinions or correct mistakes!