CREATE OR REPLACE TYPE ty_payment_type AS OBJECT
(
  payment_id   NUMBER(10),
  payment_name VARCHAR2(50),
  
  CONSTRUCTOR FUNCTION ty_payment_type RETURN SELF AS RESULT
)
/
CREATE OR REPLACE TYPE BODY ty_payment_type IS
  
  CONSTRUCTOR FUNCTION ty_payment_type RETURN SELF AS RESULT IS
  BEGIN

    RETURN;
  END ty_payment_type;
  
END;
/
