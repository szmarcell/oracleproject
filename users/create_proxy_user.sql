PROMPT Creating proxy user for kameleon...

---------------------------------------------------------------
--               Log off proxy user if logged in             --
---------------------------------------------------------------
DECLARE
  CURSOR cur IS
    SELECT 'alter system kill session ''' || SID || ',' || serial# || '''' AS command
      FROM v$session
      WHERE username = 'KAMELEON_USER';
BEGIN
  FOR c IN cur LOOP
    EXECUTE IMMEDIATE c.command;
  END LOOP;
END;
/

---------------------------------------------------------------
--                Delete proxy user if exists                --
---------------------------------------------------------------

DECLARE 
  v_count NUMBER;
BEGIN
  SELECT COUNT(*) INTO v_count FROM dba_users t WHERE t.username = 'KAMELEON_USER';
  IF v_count = 1 THEN
    EXECUTE IMMEDIATE 'DROP USER kameleon_user CASCADE';
  END IF;
END;
/


---------------------------------------------------------------
--                   Create kameleon user                    --
---------------------------------------------------------------

CREATE USER kameleon_user
  IDENTIFIED BY "12345678"
/

---------------------------------------------------------------
--              Create role for the proxy user               --
---------------------------------------------------------------

DECLARE 
  v_count NUMBER;
BEGIN
  SELECT COUNT(*) INTO v_count FROM dba_roles t WHERE t.role = 'APP_USER';
  IF v_count = 1 THEN
    EXECUTE IMMEDIATE 'DROP ROLE app_user';
  END IF;
END;
/

CREATE ROLE app_user;

GRANT CREATE SESSION TO app_user;

GRANT app_user to kameleon_owner;

ALTER USER kameleon_owner
  GRANT CONNECT THROUGH kameleon_owner
  WITH ROLE app_user;
/
