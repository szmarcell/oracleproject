PROMPT Creating user kameleon_owner...

---------------------------------------------------------------
--            Log off kameleon_owner if logged in            --
---------------------------------------------------------------
DECLARE
  CURSOR cur IS
    SELECT 'alter system kill session ''' || SID || ',' || serial# || '''' AS command
      FROM v$session
      WHERE username = 'KAMELEON_OWNER';
BEGIN
  FOR c IN cur LOOP
    EXECUTE IMMEDIATE c.command;
  END LOOP;
END;
/

---------------------------------------------------------------
--               Delete kameleon_owner if exist              --
---------------------------------------------------------------

DECLARE 
  v_count NUMBER;
BEGIN
  SELECT COUNT(*) INTO v_count FROM dba_users t WHERE t.username = 'KAMELEON_OWNER';
  IF v_count = 1 THEN
    EXECUTE IMMEDIATE 'DROP USER kameleon_owner CASCADE';
  END IF;
END;
/

---------------------------------------------------------------
--                  Create kameleon_owner                    --
---------------------------------------------------------------

CREATE USER kameleon_owner
  IDENTIFIED BY "Error9711!"
  DEFAULT TABLESPACE kameleon_tablespace
  QUOTA UNLIMITED ON kameleon_tablespace
;

---------------------------------------------------------------
--               Create role to kameleon_owner               --
---------------------------------------------------------------

DECLARE 
  v_count NUMBER;
BEGIN
  SELECT COUNT(*) INTO v_count FROM dba_roles t WHERE t.role = 'APP_OWNER';
  IF v_count = 1 THEN
    EXECUTE IMMEDIATE 'DROP ROLE app_owner';
  END IF;
END;
/

CREATE ROLE app_owner;

GRANT CREATE SESSION TO app_owner;
GRANT CREATE TABLE TO app_owner;
GRANT CREATE VIEW TO app_owner;
GRANT CREATE SEQUENCE TO app_owner;
GRANT CREATE PROCEDURE TO app_owner;
GRANT CREATE SYNONYM TO app_owner;
GRANT CREATE TRIGGER TO app_owner;
GRANT CREATE TYPE TO app_owner;
GRANT CREATE JOB TO app_owner;
GRANT EXECUTE ON dbms_scheduler TO app_owner;

GRANT app_owner TO kameleon_owner;

PROMPT Done.
