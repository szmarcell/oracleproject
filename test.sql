BEGIN
  PKG_DML_DATA_INPUT.add_supplier(p_company_name => 'p�lda',
                                  p_tax_number   => '1212121-1-14',
                                  p_loc_address  => 'Valahol',
                                  p_loc_city     => 'Valahol',
                                  p_loc_region   => 'Valahol',
                                  p_postal_code  => 5484,
                                  p_country      => 'Valahol',
                                  p_tel          => '1616/161616',
                                  p_email        => 'valami@pelda.hu');

  PKG_DML_DATA_INPUT.add_category(p_category_name => 'p�lda', p_cat_descript => 'ez egy teszt');
END;

UPDATE suppliers
SET company_name = 'm�s'
WHERE supplier_id = 1000;

SELECT * FROM suppliers;
SELECT * FROM suppliers_hist;
