CREATE OR REPLACE VIEW vw_orders_total AS
SELECT od.order_id, od.created_on, SUM((od.unit_price * od.quantity) * ((100 - od.discount) / 100)) AS SUM
  FROM order_details od
  GROUP BY od.order_id, od.created_on;
  
CREATE OR REPLACE VIEW vw_products_count_by_supplier AS
SELECT s.company_name, COUNT(p.product_id) AS quantity_of_products
  FROM products p INNER JOIN suppliers s ON p.supplier_id = s.supplier_id
  GROUP BY s.company_name;
  
CREATE OR REPLACE VIEW vw_products_by_supplier AS
SELECT s.company_name, p.product_name
  FROM products p INNER JOIN suppliers s ON p.supplier_id = s.supplier_id;
  
CREATE OR REPLACE VIEW vw_alphabet_products AS
SELECT c.category_name, p.*
  FROM categories c INNER JOIN products p ON c.category_id = p.category_id
  ORDER BY p.product_name;
  
CREATE OR REPLACE VIEW vw_turnover_this_year AS
SELECT o.order_id, o.partner_id, o.employee_id, o.purchase_date
  FROM orders o
  WHERE EXTRACT(YEAR FROM o.purchase_date) = EXTRACT(YEAR FROM SYSDATE);
  
CREATE OR REPLACE VIEW vw_turnover_last_year AS
SELECT o.order_id, o.partner_id, o.employee_id, o.purchase_date
  FROM orders o
  WHERE EXTRACT(YEAR FROM o.purchase_date) = EXTRACT(YEAR FROM SYSDATE) - 1;
  
CREATE OR REPLACE VIEW vw_department_size AS
SELECT p.dept_name, COUNT(e.employee_id) AS workers
  FROM departments p INNER JOIN employees e ON p.dept_id = e.dept_id
  GROUP BY p.dept_name;
