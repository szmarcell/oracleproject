CREATE OR REPLACE PACKAGE pkg_query_functions IS

  FUNCTION get_max_order RETURN NUMBER;
  
  FUNCTION get_min_order RETURN NUMBER;

END;
/
CREATE OR REPLACE PACKAGE BODY pkg_query_functions IS

  FUNCTION get_max_order RETURN NUMBER IS
  v_max_order NUMBER;
  BEGIN
    SELECT MAX(vw.sum) INTO v_max_order
      FROM vw_orders_total vw;
    RETURN v_max_order;
  END get_max_order;
  
  
  FUNCTION get_min_order RETURN NUMBER IS
  v_min_order NUMBER;
  BEGIN
    SELECT MIN(vw.sum) INTO v_min_order
      FROM vw_orders_total vw;
    RETURN v_min_order;
  END get_min_order;

END;
/
