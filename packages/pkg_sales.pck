create or replace package pkg_sales is

  PROCEDURE update_sales_products;

end pkg_sales;
/
CREATE OR REPLACE PACKAGE BODY pkg_sales IS

  PROCEDURE update_sales_products
    IS
  BEGIN
    UPDATE products p
    SET p.unit_price =  p.unit_price * 0.98
    WHERE p.category_id = 1011;
  END;

END pkg_sales;
/
