CREATE OR REPLACE PACKAGE pkg_dml_data_input IS

  PROCEDURE add_supplier(
    p_company_name IN VARCHAR2,
    p_tax_number   IN VARCHAR2,
    p_loc_address  IN VARCHAR2,
    p_loc_city     IN VARCHAR2,  
    p_loc_region   IN VARCHAR2,
    p_postal_code  IN NUMBER,
    p_country      IN VARCHAR2,
    p_tel          IN VARCHAR2,
    p_email        IN VARCHAR2
    );
    
  PROCEDURE add_category(
    p_category_name IN VARCHAR2,
    p_cat_descript  IN VARCHAR2
    );
    
  PROCEDURE add_product(
    p_supplier_id       IN NUMBER,
    p_category_id       IN NUMBER,
    p_product_name      IN VARCHAR2,
    p_quantity_per_unit IN NUMBER,
    p_unit_price        IN NUMBER,
    p_units_in_stock    IN NUMBER
    );
    
  PROCEDURE add_partner(
    p_partner_id   IN NUMBER,
    p_partner_name IN VARCHAR2,
    p_tax_number   IN VARCHAR2,
    p_loc_address  IN VARCHAR2,    
    p_loc_city     IN VARCHAR2,        
    p_loc_region   IN VARCHAR2,
    p_postal_code  IN NUMBER,
    p_country      IN VARCHAR2,
    p_tel          IN VARCHAR2,
    p_email        IN VARCHAR2
    );
  
  PROCEDURE add_department(
    p_dept_name   IN VARCHAR2,
    p_tax_number  IN VARCHAR2,
    p_loc_address IN VARCHAR2,
    p_loc_city    IN VARCHAR2,        
    p_loc_region  IN VARCHAR2,
    p_postal_code IN NUMBER,
    p_country     IN VARCHAR2,
    p_tel         IN VARCHAR2,
    p_email       IN VARCHAR2
    );
    
  PROCEDURE add_employee(
    p_employee_id IN NUMBER,
    p_dept_id     IN NUMBER,   
    p_last_name   IN VARCHAR2,
    p_first_name  IN VARCHAR2,
    p_title       IN VARCHAR2,
    p_birth_date  IN DATE,
    p_hire_date   IN DATE,
    p_loc_address IN VARCHAR2,
    p_loc_city    IN VARCHAR2,
    p_postal_code IN NUMBER
    );
    
  PROCEDURE add_order(
    p_order_id      IN NUMBER,
    p_partner_id    IN NUMBER,
    p_employee_id   IN NUMBER,
    p_purchase_date IN DATE
    );
  
  PROCEDURE add_order_detail(
    p_order_det_id IN NUMBER,
    p_order_id     IN NUMBER,
    p_product_id   IN NUMBER,
    p_unit_price   IN NUMBER,
    p_quantity     IN NUMBER,
    p_discount     IN NUMBER
    );

END pkg_dml_data_input;
/
CREATE OR REPLACE PACKAGE BODY pkg_dml_data_input IS

  PROCEDURE add_supplier(
    p_company_name IN VARCHAR2,
    p_tax_number   IN VARCHAR2,
    p_loc_address  IN VARCHAR2,
    p_loc_city     IN VARCHAR2,  
    p_loc_region   IN VARCHAR2,
    p_postal_code  IN NUMBER,
    p_country      IN VARCHAR2,
    p_tel          IN VARCHAR2,
    p_email        IN VARCHAR2
    ) IS
  BEGIN
    INSERT INTO suppliers(
      company_name,
      tax_number,
      loc_address,
      loc_city,
      loc_region,
      postal_code,
      country,
      tel,
      email
    )VALUES(
      p_company_name,
      p_tax_number,
      p_loc_address,
      p_loc_city,
      p_loc_region,
      p_postal_code,
      p_country,
      p_tel,
      p_email
      );
  END add_supplier;
  
  PROCEDURE add_category(
    p_category_name IN VARCHAR2,
    p_cat_descript  IN VARCHAR2
    ) IS
  BEGIN
    INSERT INTO categories(
      category_name,
      cat_descript
    )VALUES(
      p_category_name,
      p_cat_descript
      );
  END add_category;
  
  PROCEDURE add_product(
    p_supplier_id       IN NUMBER,
    p_category_id       IN NUMBER,
    p_product_name      IN VARCHAR2,
    p_quantity_per_unit IN NUMBER,
    p_unit_price        IN NUMBER,
    p_units_in_stock    IN NUMBER
    ) IS
  BEGIN
    INSERT INTO products(
      supplier_id,
      category_id,
      product_name,
      quantity_per_unit,
      unit_price,
      units_in_stock  
    )VALUES(
      p_supplier_id,
      p_category_id,
      p_product_name,
      p_quantity_per_unit,
      p_unit_price,
      p_units_in_stock
      );
  END add_product;
  
  PROCEDURE add_partner(
    p_partner_id   IN NUMBER,
    p_partner_name IN VARCHAR2,
    p_tax_number   IN VARCHAR2,
    p_loc_address  IN VARCHAR2,    
    p_loc_city     IN VARCHAR2,        
    p_loc_region   IN VARCHAR2,
    p_postal_code  IN NUMBER,
    p_country      IN VARCHAR2,
    p_tel          IN VARCHAR2,
    p_email        IN VARCHAR2
    ) IS
  BEGIN
    INSERT INTO partners(
      partner_id,
      partner_name,
      tax_number,
      loc_address,  
      loc_city,      
      loc_region,
      postal_code,
      country,
      tel,
      email
    )VALUES(
      p_partner_id,
      p_partner_name,
      p_tax_number,
      p_loc_address,  
      p_loc_city,      
      p_loc_region,
      p_postal_code,
      p_country,
      p_tel,
      p_email
      );
  END add_partner;
  
  PROCEDURE add_department(
    p_dept_name   IN VARCHAR2,
    p_tax_number  IN VARCHAR2,
    p_loc_address IN VARCHAR2,
    p_loc_city    IN VARCHAR2,        
    p_loc_region  IN VARCHAR2,
    p_postal_code IN NUMBER,
    p_country     IN VARCHAR2,
    p_tel         IN VARCHAR2,
    p_email       IN VARCHAR2
    ) IS
  BEGIN
    INSERT INTO departments(
      dept_name,
      tax_number,
      loc_address,
      loc_city,       
      loc_region,
      postal_code,
      country,
      tel,
      email
    )VALUES(
      p_dept_name,
      p_tax_number,
      p_loc_address,
      p_loc_city,    
      p_loc_region,
      p_postal_code,
      p_country,
      p_tel,
      p_email
      );
  END add_department;
  
  PROCEDURE add_employee(
    p_employee_id IN NUMBER,
    p_dept_id     IN NUMBER,   
    p_last_name   IN VARCHAR2,
    p_first_name  IN VARCHAR2,
    p_title       IN VARCHAR2,
    p_birth_date  IN DATE,
    p_hire_date   IN DATE,
    p_loc_address IN VARCHAR2,
    p_loc_city    IN VARCHAR2,
    p_postal_code IN NUMBER
    ) IS
  BEGIN
    INSERT INTO employees(
      employee_id,
      dept_id,
      last_name,
      first_name,
      title,
      birth_date,
      hire_date,
      loc_address,
      loc_city,
      postal_code
    )VALUES(
      p_employee_id,
      p_dept_id,  
      p_last_name,
      p_first_name,
      p_title,
      p_birth_date,
      p_hire_date,
      p_loc_address,
      p_loc_city,
      p_postal_code
      );
  END add_employee;
  
  PROCEDURE add_order(
    p_order_id      IN NUMBER,
    p_partner_id    IN NUMBER,
    p_employee_id   IN NUMBER,
    p_purchase_date IN DATE
    ) IS
  BEGIN
    INSERT INTO orders(
      order_id,
      partner_id,
      employee_id,
      purchase_date
    )VALUES(
      p_order_id,
      p_partner_id,
      p_employee_id,
      p_purchase_date
      );
  END add_order;
  
  PROCEDURE add_order_detail(
    p_order_det_id IN NUMBER,
    p_order_id     IN NUMBER,
    p_product_id   IN NUMBER,
    p_unit_price   IN NUMBER,
    p_quantity     IN NUMBER,
    p_discount     IN NUMBER
    ) IS
  BEGIN
    INSERT INTO order_details(
      order_det_id,
      order_id,
      product_id,
      unit_price,
      quantity,
      discount  
    )VALUES(
      p_order_det_id,
      p_order_id,
      p_product_id,
      p_unit_price,
      p_quantity,
      p_discount
      );
  END add_order_detail;

END pkg_dml_data_input;
/
