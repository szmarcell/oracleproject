CREATE OR REPLACE PACKAGE pkg_dml_data_deletion IS

  PROCEDURE delete_supplier(p_supplier_id NUMBER);
  
  PROCEDURE delete_category(p_category_id NUMBER);
  
  PROCEDURE delete_supplier(p_product_id NUMBER);
    
  PROCEDURE delete_supplier(p_partner_id NUMBER);
      
  PROCEDURE delete_supplier(p_dept_id NUMBER);
        
  PROCEDURE delete_supplier(p_employee_id NUMBER);
          
  PROCEDURE delete_supplier(p_order_id NUMBER);
            
  PROCEDURE delete_supplier(p_order_det_id NUMBER);

END pkg_dml_data_deletion;
/
CREATE OR REPLACE PACKAGE BODY pkg_dml_data_deletion IS
  
  PROCEDURE delete_supplier(p_supplier_id  NUMBER)
    IS
  BEGIN
    DELETE FROM suppliers s
      WHERE s.supplier_id = p_supplier_id;
  END;
  
  PROCEDURE delete_category(p_category_id NUMBER)
    IS
  BEGIN
    DELETE FROM categories c
      WHERE c.category_id = p_category_id;
  END;
  
  PROCEDURE delete_supplier(p_product_id NUMBER)
    IS
  BEGIN
    DELETE FROM products p
      WHERE p.product_id = p_product_id;
  END;
  
  PROCEDURE delete_supplier(p_partner_id NUMBER)
    IS
  BEGIN
    DELETE FROM partners p
      WHERE p.partner_id = p_partner_id;
  END;
  
  PROCEDURE delete_supplier(p_dept_id NUMBER)
    IS
  BEGIN
    DELETE FROM departments d
      WHERE d.dept_id = p_dept_id;
  END;
        
  PROCEDURE delete_supplier(p_employee_id NUMBER)
    IS
  BEGIN
    DELETE FROM employees e
      WHERE e.employee_id = p_employee_id;
  END;
    
  PROCEDURE delete_supplier(p_order_id NUMBER)
    IS
  BEGIN
    DELETE FROM orders o
      WHERE o.order_id = p_order_id;
  END;
  
  PROCEDURE delete_supplier(p_order_det_id NUMBER)
    IS
  BEGIN
    DELETE FROM order_details od
      WHERE od.order_det_id = p_order_det_id;
  END;
  
END pkg_dml_data_deletion;
/
