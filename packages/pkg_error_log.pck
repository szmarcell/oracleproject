CREATE OR REPLACE PACKAGE pkg_error_log IS

  PROCEDURE log_error(p_error_message IN VARCHAR2,
                      p_err_value     IN VARCHAR2,
                      p_api           IN VARCHAR2);

END pkg_error_log;
/
CREATE OR REPLACE PACKAGE BODY pkg_error_log IS

  PROCEDURE log_error(p_error_message IN VARCHAR2,
                      p_err_value     IN VARCHAR2,
                      p_api           IN VARCHAR2) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN 
    INSERT INTO error_log
      (err_id,
       err_time,
       err_message,
       err_value,
       api)
    VALUES
      (err_seq.nextval,
       sysdate,
       p_error_message,
       p_err_value,
       p_api);
       
    COMMIT;
  END log_error;

END pkg_error_log;
/
