CREATE OR REPLACE PACKAGE pkg_product_supplier IS

  PROCEDURE product_change_supplier(p_product_id IN NUMBER, p_new_supplier_id IN NUMBER);

  FUNCTION is_valid_change RETURN BOOLEAN;

END pkg_product_supplier;
/
CREATE OR REPLACE PACKAGE BODY pkg_product_supplier IS

  g_is_valid_change BOOLEAN := FALSE;
  
  PROCEDURE product_change_supplier(p_product_id      IN NUMBER
                                   ,p_new_supplier_id IN NUMBER) IS
    v_act_supplier NUMBER;
    
    e_product_exception EXCEPTION;
    v_error_message VARCHAR2(4000) := 'Term�k nem tal�lhat�';
  BEGIN
    BEGIN
      SELECT p.supplier_id INTO v_act_supplier
        FROM products p
       WHERE p.product_id = p_product_id;
     EXCEPTION WHEN OTHERS THEN
       pkg_error_log.log_error(p_error_message => dbms_utility.format_error_backtrace,
                               p_err_value     => 'p_product_id = ' || p_product_id,
                               p_api           => 'pkg_product_supplier');
                               
       RAISE e_product_exception;
    END;

    g_is_valid_change := TRUE;
    UPDATE products p
    SET p.supplier_id = p_new_supplier_id
      WHERE p.product_id = p_product_id;
      
    g_is_valid_change := FALSE;
    EXCEPTION WHEN OTHERS THEN
    pkg_error_log.log_error(p_error_message => dbms_utility.format_error_backtrace,
                            p_err_value     => 'p_product_id = ' || p_product_id || ', ' || v_error_message,
                            p_api           => 'pkg_product_supplier');
  END;
  
  FUNCTION is_valid_change RETURN BOOLEAN IS
  BEGIN
    RETURN g_is_valid_change;
  END;

END pkg_product_supplier;
/
